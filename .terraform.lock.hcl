# This file is maintained automatically by "tofu init".
# Manual edits may be lost in future updates.

provider "registry.opentofu.org/bpg/proxmox" {
  version     = "0.55.0"
  constraints = "0.55.0"
  hashes = [
    "h1:TZeaDXmIY+NXsg+BXlkraSavTbCzojkxv6+C479U6A0=",
    "h1:pR3AqRW6LrHLus6dEUDamdAhrBoXiOpQXyco7a4ju70=",
    "zh:1ced32abcbeab6abe9bca8f544c1f20c665b19c46c418577a3ff9646090f99fa",
    "zh:3e6a94a3fe93b072ef02da36584baa8a71072cf56c3c35b947067f348018dc15",
    "zh:4f71031299a30f0811eeb465c87ba102e4d4ea8aa46ac8b047724e698c1de91f",
    "zh:55eaad880eddcd5eb6a45cb9acfaa13a0db485d5c059863ac9e12bc98cbfc565",
    "zh:62dd30966734e5d13e6fd42128ace384bbee5d8af87d57d9c35a3c09f21c8110",
    "zh:706c3d614b4cb11e9e501bab9ef846ec94a525174f99cce65351d408f992915d",
    "zh:76606e2da71ee6ee55dbd2bec6d04f24b368ad88a700d9d5e29ee28a513fd108",
    "zh:7a40881f1166e7f69baa1eff7eebd6e6ef38c1c52e0ba6325cfcde28742aa6f7",
    "zh:a47cf45a3572c251c06e70f1ca5c481ec329af447ebb509cae1f8ec7a98d13e8",
    "zh:ae2f4d8ab30680141c8897dbea641417cb434befedbbbbd233ce6340d2e7c840",
    "zh:b845c294de182f8085433b352fcdf1265a8fefd852f870624916999e3c5ed888",
    "zh:cd044cd4ebdc3800fd9200cd474f18a431b446b425ddb944a9b83c42ff34aba8",
    "zh:eef617cd2d5c4b12b01dffcd280875625b09d7a80400d5cdfe1f419f54d80df6",
    "zh:f26e0763dbe6a6b2195c94b44696f2110f7f55433dc142839be16b9697fa5597",
    "zh:feb736a5bb2785be96c39f9fbc2cd18b7b08472523f0288cae829e62c6ea68d9",
  ]
}

provider "registry.opentofu.org/hashicorp/http" {
  version = "3.4.2"
  hashes = [
    "h1:KYPSiOzUcE18IUcDI19bVFVNgp+ihVrOub5hSdtct0I=",
    "zh:02431ec117a862e219dab4d014bc4d5c158b49b18dbc31fd53c2a5a1f1a3d9c7",
    "zh:11f0c07a25e03436531ed37536f7aaba376b1dc994b6d59483030f2c17120752",
    "zh:23aa3da0bf34f92fa2253b3228e8aa42aa2a2b8b27cec487bf78147aec6c373b",
    "zh:29823a543f553a61a21c9d9c0440978760e3fc2c9d017c381af20b9729621a32",
    "zh:2add06181c808f777db807cbfcb30fe3869d137ca7ad4e6ffb9f76decfe0b2c5",
    "zh:347906acb8d672d51306f6ba129d553b349960afbb22e852e33695d3bd69c2ee",
    "zh:3d389638498ee30296832b7416ffe8704820b0874522e4567aa5aca70d4354b3",
    "zh:7b855e39e47e4974b8e749fd4dadc4d06d797dac5d3aa48dff38609b1bad4c85",
    "zh:a7873dc733bf8680c312c5d0345e3646e6a1db07bc5a58bdc47b507d78c6ed56",
    "zh:ccc0af216be2200190f04859f844e989b710a65a423f126b191f3919adbe807d",
  ]
}
