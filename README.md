# proxmox-env

Proxmox configs for my homelab, using OpenTofu.

## Env Vars

- `TF_HTTP_PASSWORD` - gitlab deploy token
- `TF_VAR_proxmox_username`
- `TF_VAR_proxmox_password`
- `TF_VAR_proxmox_api_token` - `<uuid>`
