{
  description = "proxmox-env";

  inputs.nixpkgs.url = "github:NixOS/nixpkgs";
  inputs.flake-utils.url = "github:numtide/flake-utils";

  outputs = inputs @ {
    self,
    nixpkgs,
    flake-utils,
  }:
    flake-utils.lib.eachDefaultSystem (system: let
      pkgs = nixpkgs.legacyPackages.${system};
    in {
      devShell = pkgs.mkShell {
        name = "proxmox-env";
        buildInputs = with pkgs; [
          opentofu
        ];
        shellHook = ''
          EXPORTED="$(${pkgs.glab}/bin/glab variable export)"
            PLUSVARS="$(echo -n "GITLAB VARS: "; echo "$EXPORTED" | ${pkgs.jq}/bin/jq -r '.[] | "+" + .key' | tr '\n' ' '; echo)"
            SETVARS="$(echo "$EXPORTED" | ${pkgs.jq}/bin/jq -r '.[] | "export " + .key + "=" + .value')"
            echo "$PLUSVARS"
            eval "$SETVARS"
        '';
      };

      formatter = pkgs.alejandra;
    });
}
