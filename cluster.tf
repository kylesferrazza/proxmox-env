resource "proxmox_virtual_environment_cluster_options" "nucs_options" {
  language                  = "en"
  keyboard                  = "en-us"
  email_from                = "proxmox@kylesferrazza.com"
  bandwidth_limit_migration = 555555
  bandwidth_limit_default   = 666666
  max_workers               = 5
  migration_cidr            = "10.99.0.0/16"
  migration_type            = "secure"
  next_id = {
    lower = 100
    upper = 999999999
  }
}
