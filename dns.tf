resource "proxmox_virtual_environment_dns" "dns_configuration" {
  count = 3
  domain    = "home.kylesferrazza.com"
  node_name = "n${count.index+1}"

  servers = [
    "10.99.0.1"
  ]
}
