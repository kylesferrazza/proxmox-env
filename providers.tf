terraform {
  required_providers {
    proxmox = {
      source = "bpg/proxmox"
      version = "0.55.0"
    }
  }
  backend "http" {
    address = "https://gitlab.com/api/v4/projects/57553224/terraform/state/prod"
    lock_address = "https://gitlab.com/api/v4/projects/57553224/terraform/state/prod/lock"
    unlock_address = "https://gitlab.com/api/v4/projects/57553224/terraform/state/prod/lock"
    username = "deploy"
    lock_method = "POST"
    unlock_method = "DELETE"
    retry_wait_min = 5
  }
}

provider "proxmox" {
  endpoint = "https://n1.home.kylesferrazza.com:8006/"
  api_token = "${var.proxmox_username}@pam!tofu=${var.proxmox_api_token}"

  insecure = true
  tmp_dir  = "/var/tmp"

  ssh {
    agent = true
    username = var.proxmox_username
    password = var.proxmox_password
  }
}
