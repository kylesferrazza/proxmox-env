resource "proxmox_virtual_environment_pool" "templates" {
  comment = "VM templates"
  pool_id = "templates"
}

resource "proxmox_virtual_environment_pool" "k8s" {
  comment = "Kubernetes resources"
  pool_id = "k8s"
}
