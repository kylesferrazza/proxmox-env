resource "proxmox_virtual_environment_vm" "ubuntu_noble_template" {
  name        = "ubuntu-noble-template"
  description = "Managed by Terraform"
  tags        = ["terraform", "ubuntu"]

  node_name = "n1"
  vm_id     = 8001

  agent {
    enabled = true
  }

  disk {
    datastore_id = "ceph-pool"
    file_id      = proxmox_virtual_environment_download_file.ubuntu_noble_cloudimg.id
    interface    = "scsi0"
  }

  initialization {
    ip_config {
      ipv4 {
        address = "dhcp"
      }
    }

    user_account {
      keys     = [trimspace(data.http.vm_pubkey.response_body)]
      username = "kyle"
    }

    user_data_file_id = proxmox_virtual_environment_file.cloud_config_qemu_guest_agent.id
  }

  network_device {
    bridge = "LAN"
  }

  operating_system {
    type = "l26"
  }

  serial_device {}

  memory {
    dedicated = 8192
  }

  vga {
    type = "qxl"
    memory = 32
  }

  template = true
}

data "http" "vm_pubkey" {
  url = "https://f.kyle.technology/vm.pub"
}

resource "proxmox_virtual_environment_download_file" "ubuntu_noble_cloudimg" {
  content_type = "iso"
  datastore_id = "cephfs"
  node_name    = "n1"
  file_name = "noble-server-cloudimg-amd64.img"
  url = "https://cloud-images.ubuntu.com/noble/current/noble-server-cloudimg-amd64.img"
}

resource "proxmox_virtual_environment_file" "cloud_config_qemu_guest_agent" {
  content_type = "snippets"
  datastore_id = "cephfs"
  node_name = "n1"
  source_raw {
    file_name = "qemu-guest-agent.yml"
    data = <<EOF
#cloud-config

packages:
  - unattended-upgrades
  - qemu-guest-agent

runcmd:
  - |
    systemctl disable --now systemd-resolved
    unlink /etc/resolv.conf
    echo "nameserver 10.99.0.1" | tee /etc/resolv.conf
    systemctl enable --now unattended-upgrades
  - - systemctl
    - enable
    - --now
    - qemu-guest-agent.service
users:
  - name: kyle
    gecos: Kyle Sferrazza
    primary_group: kyle
    groups: users, admin
    sudo: true
    ssh_authorized_keys:
      - ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIDtb+JIbadcrIYvBoKvBOlujq+mZjMQ7YslYf6fVzO92
package_update: true
EOF
  }
}

